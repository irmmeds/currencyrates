﻿namespace CurrencyRates.Services
{
	using CurrencyRates.Models.Public.Representations;
	using System;
	using System.Threading.Tasks;

	public interface IExchangeRatesService
	{
		Task<CurrencyRatesResultRepresentation> GetCurrencyRatesByDateAsync(DateTime? date);
	}
}
