﻿namespace CurrencyRates.Services
{
	using CurrencyRates.Mappers;
	using CurrencyRates.Models.Public.Representations;
	using CurrencyRates.WebServices.ExchangeRatesLB;
	using Newtonsoft.Json;
	using System;
	using System.Threading.Tasks;

	public class ExchangeRatesService : IExchangeRatesService
	{
		private readonly ExchangeRatesSoapClient soapClient;

		public ExchangeRatesService()
		{
			soapClient = new ExchangeRatesSoapClient("ExchangeRatesSoap");
		}

		public async Task<CurrencyRatesResultRepresentation> GetCurrencyRatesByDateAsync(DateTime? date)
		{
			date = date ?? new DateTime(2014, DateTime.Now.Month, DateTime.Now.Day);
			var xmlNode = await soapClient.getExchangeRatesByDateAsync(date.ToString());
			return JsonConvert
				.DeserializeObject<CurrencyRatesResponseRepresentation>(JsonConvert.SerializeXmlNode(xmlNode))
				.MapResult(xmlNode.InnerText);
		}
	}
}
