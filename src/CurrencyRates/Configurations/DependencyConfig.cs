﻿namespace CurrencyRates.Configurations
{
	using Autofac;
	using Autofac.Integration.Mvc;
	using CurrencyRates.Services;
	using System.Reflection;
	using System.Web.Mvc;

	public class DependencyConfig
	{
		protected readonly static ContainerBuilder builder = new ContainerBuilder();

		public static void RegisterDependencies(params Assembly[] controllerAssemblies)
		{
			builder.RegisterControllers(controllerAssemblies);
			DependencyResolver.SetResolver(new AutofacDependencyResolver(RegisterDependencies()));
		}

		protected static IContainer RegisterDependencies()
		{
			builder.RegisterType<ExchangeRatesService>().As<IExchangeRatesService>().InstancePerRequest();
			return builder.Build();
		}
	}
}
