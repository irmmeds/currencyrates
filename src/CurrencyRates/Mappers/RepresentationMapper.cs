﻿using CurrencyRates.Models.Public.Representations;
using System.Linq;

namespace CurrencyRates.Mappers
{
	public static class RepresentationMapper
	{
		public static CurrencyRatesResultRepresentation MapResult(this CurrencyRatesResponseRepresentation currencyExchangeRatesRepresentation, string message = null)
		{
			if(currencyExchangeRatesRepresentation.ExchangeRates == null) {
				return new CurrencyRatesResultRepresentation
				{
					IsSucced = false,
					Message = message
				};
			}  else {
				return new CurrencyRatesResultRepresentation
				{
					CurrencyRates = currencyExchangeRatesRepresentation.ExchangeRates.Item.OrderByDescending(x => x.Rate)
				};
			}
		}
	}
}
