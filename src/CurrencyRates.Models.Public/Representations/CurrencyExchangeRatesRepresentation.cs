﻿namespace CurrencyRates.Models.Public.Representations
{
	using System.Collections.Generic;

	public class CurrencyExchangeRatesRepresentation
	{
		public List<CurrencyRatesRepresentation> Item { get; set; }
	}
}
