﻿
namespace CurrencyRates.Models.Public.Representations
{
	using System.Collections.Generic;

	public class CurrencyRatesResultRepresentation
	{
		public string Message  { get; set; }

		public bool IsSucced { get; set; }

		public IEnumerable<CurrencyRatesRepresentation> CurrencyRates { get; set; }
	}
}
