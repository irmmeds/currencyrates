﻿namespace CurrencyRates.Models.Public.Representations
{
	public class CurrencyRatesResponseRepresentation
	{
		public CurrencyExchangeRatesRepresentation ExchangeRates { get; set; }
	}
}
