﻿namespace CurrencyRates.WebApp
{
	using System.Web.Mvc;
	using System.Web.Routing;

	public class RouteConfig
	{
		public static void RegisterRoutes(RouteCollection routes)
		{
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
			routes.IgnoreRoute("Home");
			routes.IgnoreRoute("Home/Index");

			routes.MapRoute(
				name: "Default",
				url: "{controller}/{action}/{date}",
				defaults: new { controller = "Home", action = "Index", date = UrlParameter.Optional, }
			);
		}
	}
}
