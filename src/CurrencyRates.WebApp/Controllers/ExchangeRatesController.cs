﻿namespace CurrencyRates.WebApp.Controllers
{
	using CurrencyRates.Services;
	using System;
	using System.Threading.Tasks;
	using System.Web.Mvc;

	public class ExchangeRatesController : Controller
    {
		private readonly IExchangeRatesService exchangeRatesService;

		public ExchangeRatesController(IExchangeRatesService exchangeRatesService)
		{
			this.exchangeRatesService = exchangeRatesService;
		}

		[OutputCache(Duration = int.MaxValue)]
		public async Task<ActionResult> GetByDateAsync(DateTime? date)  
			=> PartialView("ExchangeRatesPartialView", await exchangeRatesService.GetCurrencyRatesByDateAsync(date));
	}
}