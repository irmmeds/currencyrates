﻿namespace CurrencyRates.WebApp.Controllers
{
	using System;
	using System.Threading.Tasks;
	using System.Web.Mvc;
	using CurrencyRates.Services;

	public class HomeController : Controller
	{
		private readonly IExchangeRatesService exchangeRatesService;

		public HomeController(IExchangeRatesService exchangeRatesService)
		{
			this.exchangeRatesService = exchangeRatesService;
		}

		public async Task<ActionResult> Index(DateTime? date) 
			=> View(await exchangeRatesService.GetCurrencyRatesByDateAsync(date));
	}
}