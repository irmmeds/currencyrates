﻿$(function () {

	setUpDatePicker();

	function setUpDatePicker() {

		var dateDefault = { year: 2014, month: new Date().getMonth(), day: new Date().getDay() };
		initializeDatePicker();

		function filterByDate(date) {
			var dateParam = moment(new Date(date)).format('YYYY-MM-DD');
			$.get("exchangeRates/getByDateAsync?date=" + dateParam)
				.done(function (response) {
					$(".panel-body").html(response);
					initializeDatePicker();
				});
		}

		function initializeDatePicker() {
			$('.input-append.date').datepicker({
				autoclose: true,
				defaultViewDate: dateDefault,
			}).on('changeDate', function (e) {
				filterByDate(e.date);
				dateDefault = { year: e.date.getFullYear(), month: e.date.getMonth(), day: e.date.getDay() }
			});
		}
	}
});